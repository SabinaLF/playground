const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
const port = 3002 || 3001;

app.use(cors());
app.use(bodyParser.json());

app.get("/tab1", (req, res) =>
  res.sendFile("./data/tab1.json", { root: __dirname })
);
app.get("/tab2", (req, res) =>
  res.sendFile("./data/tab2.json", { root: __dirname })
);
app.get("/tab3", (req, res) =>
  res.sendFile("./data/tab3.json", { root: __dirname })
);

app.listen(port, () => console.log(`Listening on port ${port}`));
