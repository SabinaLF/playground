export const scrollIntoBanners = id => {
    const HTMLElements = Array.from(document.getElementsByClassName('section'));
    const elements = HTMLElements.filter(e => e.id === id);
    for (let element of elements) {
        element.scrollIntoView({ block: "center", behavior: 'smooth' });
    }
}