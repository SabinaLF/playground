import React, { useEffect, useState } from 'react';
import loader from './assets/loader.png';

import Home from './components/Home';
import Navbar from './components/Navbar';

const App = () => {
  const [data, setData] = useState([]);
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    setIsError(false);
    setIsLoading(true);
    let urls = ['/tab1', '/tab2', '/tab3'];
    try {
      await Promise.all(urls.map(url => fetch(url)))
        .then(results => {
          Promise.all(results.map((result) => result.json()))
            .then(obj => setData(obj.map(o => o.item)))
        });
      setIsLoading(false);
    } catch (error) {
      console.log(error)
      setIsError(true);
    }
    setIsLoading(false);
  }

  useEffect(() => {
    fetchData();
  }, [])

  const handleRefresh = () => {
    setIsError(false);
  }

  if (isError) {
    return (
      <div className="Error"><span>There was an Error while loading some data. Please refresh the page.</span>
        <span><button className='error-submit' onClick={handleRefresh} type="button">Home</button></span>
      </div>
    )
  } else if (isLoading) {
    return <div className="App"><img src={loader} className="loader" alt="Loading..." /></div>
  } else {
    return (
      <div>
        <Navbar />
        <Home data={data} />
      </div>
    )
  }
}

export default App;
