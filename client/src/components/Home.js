import React from 'react';
import Slider from './Slider';
import Cards from './Cards';
import Cookie from './Cookie';
import Banners from './Banners';
import Accordion from './Accordion';
import Form from './Form';
import Footer from './Footer';

const Home = ({ data }) => {

    return (
        <div className="Home">
            <Slider />
            <Cards />
            <Banners />
            <Accordion data={data} />
            <Form />
            <Footer />
            <Cookie />
        </div>
    );
}
export default Home;