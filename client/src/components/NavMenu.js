import React from "react";
import { scrollIntoBanners } from "../utilities/utilities";
import { labels } from "../globals.json";

const NavMenu = () => {
  return (
    <ul className="nav-menu">
      <li onClick={() => scrollIntoBanners("weAre")}>{labels.weAre}</li>
      <li onClick={() => scrollIntoBanners("weDo")}>{labels.weDo}</li>
      <li onClick={() => scrollIntoBanners("careers")}>{labels.careers}</li>
      <li onClick={() => scrollIntoBanners("form")}>{labels.contactUs}</li>
    </ul>
  );
};
export default NavMenu;
