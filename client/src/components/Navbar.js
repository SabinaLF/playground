import React, { useState, useEffect, useRef } from 'react';
import NavMenu from './NavMenu';
import { scrollIntoBanners } from '../utilities/utilities';
import logo from '../assets/logo-playground-white.png';

const Navbar = () => {
    const prevScrollY = useRef(0);
    const [goingUp, setGoingUp] = useState(false);

    useEffect(() => {
        const handleScroll  = () => {
            const currentScrollY = window.scrollY;
            if (prevScrollY.current > currentScrollY && goingUp) { setGoingUp(true) };
            if (prevScrollY.current < currentScrollY && !goingUp) { setGoingUp(true) };
            if (currentScrollY === 0) {setGoingUp(false)}
            prevScrollY.current = currentScrollY;
        };
        window.addEventListener("scroll", handleScroll, { passive: true });
        return () => window.removeEventListener("scroll", handleScroll);
    }, [goingUp]);

    return (
        <div className="Navbar">
            <div className={goingUp ? 'container scrolled-nav' : 'container'}>
                <div className='nav-logo' onClick={() => scrollIntoBanners('slider')}>
                    <img className='logo' src={logo} alt='Playground' />
                </div>
                <NavMenu/>
            </div>
        </div>
    );
}
export default Navbar;