import React, { useState, useEffect, useCallback } from "react";
import EmblaCarouselReact from "embla-carousel-react";
import {
  DotButton,
  PrevButton,
  NextButton,
} from "../utilities/emblaCarouselButton";
import uuid from "uuid/v4";

import { hello, play, circle } from "../globals.json";
import slideHello from "../assets/slide-hello.jpg";
import slidePlay from "../assets/slide-play.jpg";
import slideCircle from "../assets/slide-circle.jpg";

const Slider = () => {
  const [embla, setEmbla] = useState(null);
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(1);
  const [scrollSnaps, setScrollSnaps] = useState([]);

  const slidesInfo = [
    { src: slideHello, header: hello.header, copy: hello.copy },
    { src: slidePlay, header: play.header, copy: play.copy },
    { src: slideCircle, header: circle.header, copy: circle.copy },
  ];

  const scrollTo = useCallback((index) => embla.scrollTo(index), [embla]);
  const scrollPrev = useCallback(() => embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla.scrollNext(), [embla]);

  useEffect(() => {
    const onSelect = () => {
      setSelectedIndex(embla.selectedScrollSnap());
      setPrevBtnEnabled(embla.canScrollPrev());
      setNextBtnEnabled(embla.canScrollNext());
    };
    if (embla) {
      setScrollSnaps(embla.scrollSnapList());
      embla.on("select", onSelect);
      onSelect();
    }
    return () => embla && embla.destroy();
  }, [embla]);

  const slides = slidesInfo.map((s) => {
    return (
      <div key={uuid()} className="slide">
        <div className="slide-text">
          <h3 className="slide-header">{s.header}</h3>
          <h3 className="slide-copy">{s.copy}</h3>
        </div>
        <img src={s.src} alt="Background" />
      </div>
    );
  });

  return (
    <div className="Slider section" id="slider">
      <EmblaCarouselReact
        emblaRef={setEmbla}
        options={{ loop: false, draggable: false }}
      >
        <div style={{ display: "flex" }}>{slides}</div>
      </EmblaCarouselReact>
      <div className="embla__dots">
        {scrollSnaps.map((snap, index) => (
          <DotButton
            selected={index === selectedIndex}
            onClick={() => scrollTo(index)}
            key={index}
          />
        ))}
      </div>
      <PrevButton onClick={scrollPrev} enabled={prevBtnEnabled} />
      <NextButton onClick={scrollNext} enabled={nextBtnEnabled} />
    </div>
  );
};
export default Slider;
