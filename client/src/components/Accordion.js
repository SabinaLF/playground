import React from 'react';
import uuid from 'uuid';
import { Tabs, useTabState, usePanelState } from "@bumaga/tabs";

const Tab = ({ children }) => {
    const { onClick } = useTabState();
    return <button className='tabs' onClick={onClick}>{children}</button>;
};

const Panel = ({ children }) => {
    const isActive = usePanelState();
    return isActive ? <p className='panels'>{children}</p> : null;
};

const Accordion = ({ data }) => {
    const childrenTabs = data.map(child => <Tab key={uuid()}>{child.title}</Tab>);
    const childrenPanels = data.map(child => <Panel key={uuid()}>{child.content}</Panel>);

    return (
        <div className="Accordion">
            <Tabs>
                <div className='accordion-container'>
                    <div className='accordion-tabs'>{childrenTabs}</div>
                    <div className='accordion-panels'>{childrenPanels}</div>
                </div>
            </Tabs>
        </div>
    );
}
export default Accordion;