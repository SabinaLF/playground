import React from 'react';
import NavMenu from './NavMenu';
import logo from '../assets/logo-footer.png';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faLinkedinIn, faInstagram } from '@fortawesome/free-brands-svg-icons';

library.add(faFacebook, faLinkedinIn, faInstagram); 



const Footer = () => {

    return (
        <div className="Footer">
            <div className="logo-footer"><img src={logo} alt="Playground Logo"/></div>
            <div className="menu-footer"><NavMenu /></div>
            <div className="icons">
                <FontAwesomeIcon className="icon" icon={['fab', 'facebook']} />
                <FontAwesomeIcon className="icon" icon={['fab', 'linkedin-in']} />
                <FontAwesomeIcon className="icon" icon={['fab', 'instagram']} />
            </div>
        </div>
    );
}
export default Footer;