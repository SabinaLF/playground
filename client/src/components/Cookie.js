import React, { useState } from "react";
import { cookie } from "../globals.json";

const Cookie = () => {
  const [cookieOpen, setCookieOpen] = useState(true);
  const handleCloseCookie = () => setCookieOpen(false);

  return (
    cookieOpen && (
      <div className="Cookie">
        <p className="cookie-copy">
          {cookie.copy}
          <a href="./Home.js">{cookie.link}</a>
        </p>
        <button className="cookie-button" onClick={handleCloseCookie}>
          {cookie.button}
        </button>
      </div>
    )
  );
};
export default Cookie;
