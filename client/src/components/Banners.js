import React from "react";
import ImgOne from "../assets/banner-we-are.jpg";
import ImgTwo from "../assets/banner-we-do.jpg";
import ImgThree from "../assets/banner-careers.jpg";
import BannerCopy from "./BannerCopy";
import { players, projects, positions } from "../globals.json";

const Banners = () => {
  return (
    <div className="Banners">
      <div id="weAre" className="img1 section">
        <img className="banner-images" src={ImgOne} alt="We are" />
      </div>
      <div className="copy1">
        <BannerCopy
          title={players.title}
          content={players.content}
          button={players.button}
        />
      </div>
      <div className="copy2">
        <BannerCopy
          title={projects.title}
          content={projects.content}
          button={projects.button}
        />
      </div>
      <div id="weDo" className="img2 section">
        <img className="banner-images" src={ImgTwo} alt="We do" />
      </div>
      <div id="careers" className="img3 section">
        <img className="banner-images" src={ImgThree} alt="Careers" />
      </div>
      <div className="copy3">
        <BannerCopy
          title={positions.title}
          content={positions.content}
          button={positions.button}
        />
      </div>
    </div>
  );
};
export default Banners;
