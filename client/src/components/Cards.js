import React from "react";
import uuid from "uuid/v4";
import { labels, playHarder, simplicity, innovation } from "../globals.json";

import cardOne from "../assets/card-play-harder.jpg";
import cardTwo from "../assets/card-simplicity.jpg";
import cardThree from "../assets/card-innovation.jpg";

const Cards = () => {
  const cardsInfo = [
    { src: cardOne, header: playHarder.header, copy: playHarder.copy },
    { src: cardTwo, header: simplicity.header, copy: simplicity.copy },
    { src: cardThree, header: innovation.header, copy: innovation.copy },
  ];

  const cards = cardsInfo.map((c) => {
    return (
      <div key={uuid()} className="card">
        <div className="card-container">
          <div
            className="card-header"
            style={{ backgroundImage: `url(${c.src})` }}
          >
            {c.header}
          </div>
          <div className="card-copy">{c.copy}</div>
        </div>
        <button className="card-button">{labels.readMore}</button>
      </div>
    );
  });

  return <div className="Cards">{cards}</div>;
};
export default Cards;
