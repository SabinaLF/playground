import React from "react";
import { labels } from "../globals.json";

const Form = () => {
  return (
    <div className="Form section" id="form">
      <div className="contact-info">
        <div className="contact-header">{labels.contactUs}.</div>
        <div className="contact-copy">
          <p>{labels.companyName}</p>
          <p>{labels.companyAddress}</p>
          <p>{labels.companyCity}</p>
          <p>-</p>
          <p>{labels.companyPhoneNumber}</p>
        </div>
      </div>
      <form className="contact-form">
        <input className="inputs" id="first-name" placeholder="first name" />
        <input className="inputs" id="last-name" placeholder="last name" />
        <input className="inputs" id="email" placeholder="email" />
        <textarea
          className="inputs"
          id="message"
          placeholder="message"
        ></textarea>
        <button className="submit" type="submit">
          {labels.formButton}
        </button>
      </form>
    </div>
  );
};
export default Form;
