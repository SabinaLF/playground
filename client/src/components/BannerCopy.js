import React from 'react';

const BannerCopy = ({ title, content, button }) => {

    return (
        <div className="BannerCopy">
            <h3 className='banner-title'>{title}</h3>
            <p className='banner-content'>{content}</p>
            <button className='banner-button'>{button}</button>
        </div>
    );
}
export default BannerCopy;